import Axios from 'axios';
import { useState } from 'react'

function App() {

  const [nisitid, setNisitID] = useState(0);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [p_date, setP_date] = useState("");
  const [p_time, setP_time] = useState("");
  const [note, setNote] = useState("");
  const [selectedfile, setSelectedFile] = useState(null);

  const [nisitList, setNisitList] = useState([]);

  const [submitButton, setSubmitButton] = useState(false);

  const [show, setShow] = useState(false);
  const showResult = () => setShow(true);
  const hideResult = () => setShow(false);
  const [disable, setDisable] = useState(true);

  const [searchTerm, setSearchTerm] = useState("");


  const getNisit = () => {
    Axios.get('https://nisit-cpe-payments.herokuapp.com/nisitpayments').then(response => {
      setNisitList(response.data);
    });
  }

  const upload = () => {
    if(window.confirm("Your file has been uploaded! Please Submit in 3 seconds")){
      setSubmitButton(true);
    }
    Axios.post('https://nisit-cpe-payments.herokuapp.com/upload',{ 
      selectedfile: selectedfile,
    }).then(() => {
      setSelectedFile([
        ...selectedfile,{
          selectedfile: selectedfile,
        }
      ])
    })
  }

  const summit = () => {
    //const fd = new FormData();  
    //fd.append('image', selectedfile, selectedfile.name);
    Axios.post("https://nisit-cpe-payments.herokuapp.com/create", {
      nisitid: nisitid,
      name: name,
      email: email,
      p_date: p_date,
      p_time: p_time,
      selectedfile: selectedfile,
      note: note,
    }).then(() => {
      setNisitList([
        ...nisitList,
        {
          nisitid: nisitid,
          name: name,
          email: email,
          p_date: p_date,
          p_time: p_time,
          selectedfile: selectedfile,
          note: note,
        },
      ])
    });
    if(window.confirm("Submitted successfully! you can now exit this page.")){
      window.close();
    }
  }


  return (
    <div className="App">

      <div className="container">
      <div className="content">
        <img src="metrics-animate.svg"></img>
        <h3>We're CPE KUSRC82!</h3>
      </div>
      <div className="main-content">
      <h2 className="h2-title">submitting weekly payment evidence</h2>
      
      <div className="information">
      <form encType="multipart/form-data" action="https://nisit-cpe-payments.herokuapp.com/upload" method="post">
          <div className="mb-3">
          <div className="field field_v2">
            <label htmlFor="nisitid" className="ha-screen-reader">Nisit ID</label>
            <input type="text" name="nisitid" className="field__input" placeholder="6530******" onChange={(event => setNisitID(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Nisit ID<span className="alt_description_text">* <b>Important :</b> Please confirm your Nisit ID at all times *</span></span>
            </span>
          </div>
          </div>
          <div className="mb-3">
            <div className="field field_v2">
            <label htmlFor="name" className="ha-screen-reader">Name</label>
            <input type="text" className="field__input" placeholder="ชื่อ-นามสกุล (ทัยจ้าา)" onChange={(event => setName(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Name</span>
            </span>
          </div>
          </div>  
          <div className="mb-3">
          <div className="field field_v2">
            <label htmlFor="email" className="ha-screen-reader">Email</label>
            <input type="email" className="field__input" placeholder="your.email@ku.th" onChange={(event => setEmail(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Email</span>
            </span>
            </div>
          </div>  
          <div className="mb-3 date-time">
          <div className="field field_v2">
            <label htmlFor="date" className="ha-screen-reader">Date when your payment was made (08/01/2022)</label>
            <input type="date" className="field__input" onChange={(event => setP_date(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Date when payment was made</span>
            </span>
          </div>
          <div className="field field_v2">
            <label htmlFor="time" className="ha-screen-reader">Time when your payment was made</label>
            <input type="time" className="field__input" onChange={(event => setP_time(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Time when payment was made</span>
            </span>
            </div>
          </div>
          <div className="mb-3">
          <div className="field field_v2">
            <label htmlFor="note" className="ha-screen-reader">Note</label>
            <input type="text" className="field__input" placeholder="ไม่สามารถจ่ายอาทิตย์นี้ได้, อื่นๆ ( ในวันเช็คยอด จำเป็นต้องจ่ายให้ครบยอดเท่ากันทุกคน )" onChange={(event => setNote(event.target.value))}></input>
            <span className="field__label-wrap" aria-hidden="true">
            <span className="field__label">Note</span>
            </span>
          </div>
          </div>
          <div className="mb-3"> 
            <label htmlFor="slips" className="form-label">Payment Slips</label> <span className="description_text">* Filename cannot match with the last payments name *</span>
            <label htmlFor="files" className="btn upload-btn">Upload payment slips</label>
            <input id="files" name="file" type="file" className="form-control" placeholder="File" onChange={(event => setSelectedFile(nisitid + "_" + event.target.files[0].name))}></input>
          </div> 
          <div className="mb-3 button">
          <button disabled={!submitButton} name="submit" type="submit" value="submit" className="btn button-81 submit" onTouch={summit} onClick={summit}>Submit</button> 
            <button name="upload" type="submit" disabled={!nisitid || !selectedfile || !disable} value={submitButton} className="btn button-81 upload" onTouch={upload} onClick={upload}>Upload</button>
          </div>
        </form>

      </div>
      </div>
        </div>
        <div className="nisits-container" onClick={getNisit}>
        <div className="Nisits">
          <div className="nisits-button">
          <button type="button" className="btn mb-3" onTouch={showResult} onClick={showResult}>Show payments data</button>
          <button type="button" className="btn mb-3" onTouch={hideResult} onClick={hideResult}>Hide payments history</button>
          </div>
          
          <div className="nisits-search mb-3">
            <input type="text" placeholder="Search bar (Nisit ID, Name, Email)" onChange={(event) => setSearchTerm(event.target.value)}></input>
          </div>

          <div className="nisits-data">
          {nisitList.filter((val) => {
            if(searchTerm == ""){
              return val
            }else if(val.name.toLowerCase().includes(searchTerm.toLowerCase())){
              return val
            }else if(val.nisitid.toString().includes(searchTerm.toString())){
              return val
            }else if(val.email.toString().includes(searchTerm.toString())){
              return val
            }
          }).map((val, key) =>{
              return (
                show?<div className="nisit card mb-3" key={key}>
                <div className="card-body text-left">
                  <p className="card-text">Nisit ID: {val.nisitid}</p>
                  <p className="card-text">Name: {val.name}</p>
                  <p className="card-text">Email: {val.email}</p>
                  <p className="card-text">Dates: {val.p_date}</p>
                  <p className="card-text">Time: {val.p_time}</p>
                  <p className="card-text">Evidence: {val.imgdir}</p>
                </div>
                </div>:null
              )
          })}
            </div>
        </div>
        </div>
      </div>
      
      
      
  );
}

export default App;
